# RV-Commons

This library aims to make it easier to create adapters for RecyclerViews in Android-Applications.

## Usage

### BasicSimpleAdapter

This adapter allows you to fill a recyclerView without extending any adapter as is shown in the following code:

```kotlin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_adapter_demo)

        val mAdapter = BasicSimpleAdapter<Any>(this, items)

        rv_items.setHasFixedSize(true)
        rv_items.layoutManager = LinearLayoutManager(this)
        rv_items.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        rv_items.adapter = mAdapter
    }
```

### SimpleAdapter

Extend SimpleAdapter with the Type of Item you want to display and a Viewholder that extends RecyclerView.Viewholder

```java
public class MySimpleAdapter extends SimpleAdapter<Item, ItemViewHolder> {

    public MySimpleAdapter(Context context, Collection<Item> data) {
        super(context, data);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType, Context ctx) {
        return new ItemViewHolder(LayoutInflater.from(ctx).inflate(R.layout.simple_item, parent, false));
    }
    
    @Override
    public void onBindViewHolder(ItemViewHolder holder, Item object) {
        holder.title.setText(object.getName());
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;

        public ItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.simple_text);
        }
    }
}
```

Add this code in your Activity

```java
RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
MySimpleAdapter adapter = new MySimpleAdapter(this, listOfItems);
rv.setAdapter(adapter);
```

### SearchableAdapter

Extend SearchableAdapter with the Type of Item you want to display and a Viewholder that extends RecyclerView.Viewholder.

Make sure that your Item implements the Searchable-Interface.

```java
public class MySearchableeAdapter extends SearchableAdapter<Item, ItemViewHolder> {

    public MySearchableeAdapter(Context context, Collection<Item> data) {
        super(context, data);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType, Context ctx) {
        return new ItemViewHolder(LayoutInflater.from(ctx).inflate(R.layout.simple_item, parent, false));
    }
    
    @Override
    public void onBindViewHolder(ItemViewHolder holder, Item object) {
        holder.title.setText(object.getName());
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;

        public ItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.simple_text);
        }
    }
}
```

Add this code in your Activity

```java
RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerview);
MySearchableAdapter adapter = new MySearchableAdapter(this, listOfItems);
rv.setAdapter(adapter);

//you can search the adapter like this
adapter.search("Text");

```

## Setup

modify your project-level `build.gradle` file's repository definition
```groovy
repositories {
    jcenter()
    // Add these lines:
    maven {
        url 'http://publicmove.at:8081/artifactory/libs-release-local'
    }
}
```

in your module-level `build.gradle` file add the following dependency
```groovy
compile 'org.bitbucket.tuesd4y:rvcommons:1.0.1'
```

---

## Author
### Chris Stelzmüller
* [twitter](https://twitter.com/_stoez)
* [mail](mailto:office@publicmove.at)
* Currently working on [public move](http://publicmove.at) (german)
* for bugs and problems please create issues