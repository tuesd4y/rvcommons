package org.bitbucket.tuesd4y.rvcommons.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import org.bitbucket.tuesd4y.rvcommons.interfaces.Searchable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 23.11.2016
 */

public abstract class SearchableAdapter<T extends Searchable, VH extends ViewHolder> extends SimpleAdapter<T, VH> {

    private List<T> searchedItems;
    private CharSequence lastQuery = "";

    public SearchableAdapter(Context context, Collection<T> data) {
        super(context, data);
        searchedItems = new LinkedList<>(data);
    }

    public void search(CharSequence query) {
        lastQuery = query;
        searchedItems = new LinkedList<>();
        for (int i = 0; i < super.getItemCount(); i++) {
            if (getItemAt(i).matches(query)) {
                searchedItems.add(getItemAt(i));
            }
        }
    }

    @Override
    public void listUpdateCallback() {
        search(lastQuery);
    }

    @Override
    public int getItemCount() {
        return searchedItems.size();
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        onBindViewHolder(holder, searchedItems.get(position));
    }
}
