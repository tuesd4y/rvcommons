package org.bitbucket.tuesd4y.rvcommons.adapters.impl;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.bitbucket.tuesd4y.rvcommons.R;
import org.bitbucket.tuesd4y.rvcommons.adapters.SimpleAdapter;

import java.util.Collection;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 30.11.2016
 */
public class BasicSimpleAdapter<T> extends SimpleAdapter<T, BasicSimpleAdapter.BasicViewHolder> {

    public BasicSimpleAdapter(Context context, Collection<T> data) {
        super(context, data);
    }

    @Override
    public BasicViewHolder onCreateViewHolder(ViewGroup parent, int viewType, Context ctx) {
        return new BasicViewHolder(LayoutInflater.from(ctx).inflate(R.layout.text_only_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(BasicViewHolder holder, T item) {
        holder.setText(item.toString());
    }

    public static class BasicViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public void setText(CharSequence text) {
            textView.setText(text);
        }

        public BasicViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_simple_item_text);
        }
    }
}
