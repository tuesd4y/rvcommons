package org.bitbucket.tuesd4y.rvcommons.interfaces;

/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 25.11.2016
 */
public interface Searchable {

    public boolean matches(CharSequence query);

}
