package org.bitbucket.tuesd4y.testapplication

import org.bitbucket.tuesd4y.testapplication.entities.Object
import org.bitbucket.tuesd4y.testapplication.entities.Person

/**
 * Created by stoez on 30.11.2016.
 */
public object Data {
    public val persons = mutableListOf(Person(0, "Clary"), Person(1, "Jace"), Person(2, "Simon"), Person(3, "Izzy"))
    public val items = mutableListOf(
            Object(0, "Boot", "a small boot", persons[0]),
            Object(1, "Ring", "the one ring to rule them all", persons[0]),
            Object(2, "Stick", "a stick you can poke people with", persons[1]),
            Object(3, "Sword", "helps you slay people", persons[1]),
            Object(4, "Cup of Coffee", "who doesn't want a nice cup of coffee?", persons[2]),
            Object(5, "Whip", "helps you whip cream for the coffee", persons[3]),
            Object(6, "Smartphone", "because everyone needs a Smartphone these days", persons[3])
            )
}