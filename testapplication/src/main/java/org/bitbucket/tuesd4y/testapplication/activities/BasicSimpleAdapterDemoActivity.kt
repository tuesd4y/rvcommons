package org.bitbucket.tuesd4y.testapplication.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import tuesd4y.bitbucket.org.testapplication.R
import kotlinx.android.synthetic.main.activity_basic_simple_adapter_demo.rv_items
import org.bitbucket.tuesd4y.testapplication.Data
import org.bitbucket.tuesd4y.rvcommons.adapters.impl.BasicSimpleAdapter

class BasicSimpleAdapterDemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_adapter_demo)

        val mAdapter = BasicSimpleAdapter<Any>(this, (Data.items as Collection<Any>?)!! + Data.persons)

        rv_items.setHasFixedSize(true)
        rv_items.layoutManager = LinearLayoutManager(this)
        rv_items.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        rv_items.adapter = mAdapter
    }
}
