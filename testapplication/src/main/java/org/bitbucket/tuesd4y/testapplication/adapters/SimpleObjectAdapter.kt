package org.bitbucket.tuesd4y.testapplication.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.bitbucket.tuesd4y.rvcommons.adapters.SimpleAdapter
import org.bitbucket.tuesd4y.testapplication.entities.Object
import tuesd4y.bitbucket.org.testapplication.R

/**
 * Created by stoez on 30.11.2016.
 */
public class SimpleObjectAdapter(context: Context, data: MutableCollection<Object>) : SimpleAdapter<Object, SimpleObjectAdapter.SimpleItemViewHolder>(context, data) {
    override fun onBindViewHolder(vh: SimpleItemViewHolder, `object`: Object) {
        vh.name.text = `object`.name
        vh.description.text = `object`.description
        vh.ownerName.text = `object`.owner.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int, context: Context): SimpleItemViewHolder {
        return SimpleItemViewHolder(LayoutInflater.from(context).inflate(R.layout.simple_list_item, parent, false))
    }

    public class SimpleItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val name: TextView
        public val description: TextView
        public val ownerName: TextView

        init {
            name = itemView.findViewById(R.id.tv_name) as TextView
            description = itemView.findViewById(R.id.tv_description) as TextView
            ownerName = itemView.findViewById(R.id.tv_owner_name) as TextView
        }

    }
}