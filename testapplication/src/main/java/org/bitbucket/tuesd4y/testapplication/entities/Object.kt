package org.bitbucket.tuesd4y.testapplication.entities

/**
 * Created by stoez on 30.11.2016.
 */
data class Object(val id: Long, val name: String, val description: String, val owner: Person) {
    override fun toString(): String {
        return name + " (Object)"
    }
}