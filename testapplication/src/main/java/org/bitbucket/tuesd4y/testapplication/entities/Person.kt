package org.bitbucket.tuesd4y.testapplication.entities

/**
 * Created by stoez on 30.11.2016.
 */
data class Person(val id: Long, val name: String) {
    override fun toString(): String {
        return name + " (Person)"
    }
}